from django.urls import path
from api import views

urlpatterns = [
    path('products/', views.product_list),
    path('products/<int:pk>/', views.product_detail),

    path('album/create', views.AlbumCreateView.as_view()),
    path('album/all', views.AlbumListView.as_view()),
    path('album/detail/<int:pk>', views.AlbumDetailView.as_view()),

    path('track/create', views.TrackCreateView.as_view()),
    path('track/all', views.TrackListView.as_view()),
    path('track/detail/<int:pk>', views.TrackDetailView.as_view()),
]
